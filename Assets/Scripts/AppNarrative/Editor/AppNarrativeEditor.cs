
using UnityEditor;

namespace Std.AppNarrative.Editor {

    public class AppNarrativeEditor : NarrativeEditor {

        [MenuItem("Martin Dancing/Show Narrative System Editor #&n")]
        private static void Init() {

            /**
             * Register app specific actions
             */
            AppNarrativeActionTypesMap.AddAppActions();

            NarrativeEditor.ShowEditor();
        }

    }
}