using System.Collections.Generic;
using TimiNarrative.Runtime.Action;

namespace Std.AppNarrative {
    public class TestAppActionDataReader : NarrativeActionDataReaderBase {

        private List<string> _emptyList = new List<string>();
        public override List<string> GetRequiredFields() {
            return this._emptyList;
        }
    }
}