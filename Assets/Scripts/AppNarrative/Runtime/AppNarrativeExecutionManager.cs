using TimiNarrative.Runtime;

namespace Std.AppNarrative {

    public class AppNarrativeExecutionManager : NarrativeExecutionManager {

        protected override void Start() {

            /**
             * Register app specific actions
             */
            AppNarrativeActionTypesMap.AddAppActions();

            base.Start();
        }
    }
}