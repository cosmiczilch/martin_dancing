using System;
using System.Collections.Generic;
using TimiNarrative.ActionMappers;
using TimiUtils;

namespace Std.AppNarrative {

    public static class AppNarrativeActionTypesMap {

        private static Dictionary<int, Pair<Type, Type>> _map = new Dictionary<int, Pair<Type, Type>> {
            {9000, new Pair<Type, Type>(typeof(TestAppActionDataReader),
                                        typeof(TestAppActionExecutor))},
        };

        public static void AddAppActions() {
            NarrativeActionTypesMap.AddMappings(_map);
        }
    }
}