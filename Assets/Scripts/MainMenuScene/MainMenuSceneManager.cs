using MainMenuScene.UI;
using TimiNarrative.Data;
using TimiNarrative.DataModel;
using TimiNarrative.Runtime;
using TimiShared.Debug;
using UnityEngine;

namespace MainMenuScene {

    public class MainMenuSceneManager : MonoBehaviour {

        private UIMainMenuController _controller;

        public void Start() {
            this._controller = new UIMainMenuController(this.OnStartClickedHandler);
            this._controller.PresentDialog();
        }

        private void OnStartClickedHandler() {

            Book book = new Book();
            book.SetFileName("Book of Martin");
            book.Persister.Restore();

            NarrativeExecutionManager.Instance.ExecuteBook(book, -1, true);

            if (this._controller != null) {
                this._controller.RemoveDialog();
            }
        }

    }
}