using TimiShared.UI;
using UnityEngine;

namespace MainMenuScene.UI {

    public class UIMainMenuView : DialogViewBase {

        private System.Action _onStartClickedCallback = null;

        public void Configure(System.Action onStartClickedCallback) {
            this._onStartClickedCallback = onStartClickedCallback;
        }

        private void Update() {
            if (Input.GetKey(KeyCode.Space)) {
                this._onStartClickedCallback?.Invoke();
            }
        }

    }
}