using TimiShared.UI;

namespace MainMenuScene.UI {

    public class UIMainMenuController : DialogControllerBase<UIMainMenuView> {

        private const string kViewPrefabPath = "Prefabs/UI/MainMenu/UIMainMenu";
        protected override string GetDialogViewPrefabPath() {
            return kViewPrefabPath;
        }

        private System.Action _onStartClickedCallback = null;

        public UIMainMenuController(System.Action onStartClickedCallback) {
            this._onStartClickedCallback = onStartClickedCallback;
        }

        protected override void ConfigureView() {
            this.View.Configure(this.OnStartClickedHandler);
        }

        private void OnStartClickedHandler() {
            if (this._onStartClickedCallback != null) {
                this._onStartClickedCallback.Invoke();
            }
        }
    }
}