using UnityEngine;

namespace Common {

    public class CollisionNotifier : MonoBehaviour {

        private System.Action _onColissionCallback = null;
        private bool _oneShot = false;

        public void Initialize(System.Action onCollisionCallback, bool oneShot) {
            this._onColissionCallback = onCollisionCallback;
            this._oneShot = oneShot;
        }

        private void OnCollisionEnter(Collision other) {
            this.HandleColission();
        }

        private void OnCollisionEnter2D(Collision2D other) {
            this.HandleColission();
        }

        private void OnTriggerEnter(Collider other) {
            this.HandleColission();
        }

        private void OnTriggerEnter2D(Collider2D other) {
            this.HandleColission();
        }

        private void HandleColission() {
            this._onColissionCallback?.Invoke();
            if (this._oneShot) {
                Destroy(this);
            }
        }
    }
}