using TimiShared.Loading;
using TimiShared.UI;
using UnityEngine;

namespace Common {

    public class SpawnOneUpOnCollision : MonoBehaviour {

        [SerializeField] private string k1UpPrefabPath = "Prefabs/UI/Common/OneUp";

        private void OnCollisionEnter(Collision other) {
            this.HandleCollision();
        }

        private void OnCollisionEnter2D(Collision2D other) {
            this.HandleCollision();
        }

        private void OnTriggerEnter(Collider other) {
            this.HandleCollision();
        }

        private void OnTriggerEnter2D(Collider2D other) {
            this.HandleCollision();
        }

        private void HandleCollision() {
            GameObject go = PrefabLoader.Instance.InstantiateSynchronous(k1UpPrefabPath, UIRoot.Instance.MainCanvas.transform);
            if (go != null) {
                go.transform.SetPositionAndRotation(this.gameObject.transform.position, Quaternion.identity);
            }

            Destroy(this);
        }
    }
}
