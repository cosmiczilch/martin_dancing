using UnityEngine;

namespace Common {

    public class SelfDestructAfterDelay : MonoBehaviour {

        [SerializeField] private float _lifetimeSeconds = 5.0f;
        private float _ageSeconds = 0.0f;

        private void Update() {
            this._ageSeconds += Time.deltaTime;
            if (this._ageSeconds > this._lifetimeSeconds) {
                GameObject.Destroy(this.gameObject);
            }
        }
    }
}