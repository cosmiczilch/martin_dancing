﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class Player : MonoBehaviour
{
    [SerializeField] private Animator _playerAnimator;
    [SerializeField] private Rigidbody2D _rigidBody;
    [SerializeField] private AudioSource _levelUpAudio;
    [SerializeField] float _maxSpeed = 1f;

    Vector2 movement;

    private void Update() {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");

        _playerAnimator.SetFloat("horizontal", movement.x);
        _playerAnimator.SetFloat("vertical", movement.y);
        _playerAnimator.SetFloat("speed", movement.sqrMagnitude);
    }

	void FixedUpdate () {
        if(_rigidBody == null || _playerAnimator == null) {
            return;
        }

		_rigidBody.MovePosition(_rigidBody.position + (movement * _maxSpeed * Time.fixedDeltaTime));
	}

    public void PlayLevelUpAnimation() {
        this._playerAnimator.Play("LevelUpAnimation");
        this._levelUpAudio.Play();
    }
}
