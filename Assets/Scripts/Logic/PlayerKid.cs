﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class PlayerKid : Player
{
    [SerializeField] private Transform _levelUpTrigger;
    [SerializeField] private Transform _playerYoungPlaceholder;
    [SerializeField] private Player _playerYoungPrefab;

    private void Start() {
        if(this._levelUpTrigger != null) {
            this._levelUpTrigger.gameObject.AddComponent<CollisionNotifier>().Initialize(this.OnLevelUpTrigerredHandler, true);
        }
    }

    private void OnLevelUpTrigerredHandler() {
        if(this._playerYoungPlaceholder == null || this._playerYoungPrefab == null) {
            return;
        }

        GameObject playerObj = Instantiate(this._playerYoungPrefab.gameObject, this.transform.position, Quaternion.identity);
        playerObj.transform.SetParent(this._playerYoungPlaceholder, false);
        playerObj.GetComponent<Player>().PlayLevelUpAnimation();

        Destroy(this.gameObject);
    }
    
}
