﻿using TimiShared.Init;
using UnityEngine;

public class AppInit : MonoBehaviour, IInitializable {

    #region Events
    public static System.Action OnAppInitComplete = delegate {};
    #endregion

    #region IInitializable
    public void StartInitialize() {
        // Nothing to do here
        this.IsFullyInitialized = true;
        OnAppInitComplete.Invoke();
    }

    public bool IsFullyInitialized {
        get; private set;
    }

    public string GetName {
        get {
            return this.GetType().Name;
        }
    }
    #endregion

}
