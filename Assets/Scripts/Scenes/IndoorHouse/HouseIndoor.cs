﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace Scenes{
    public class HouseIndoor : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _tableTennisTrigger;
        [SerializeField] List<GameObject> _tableTennisRackets;

        private void Start() {
            this.ShowTTRackets(false);
            this._tableTennisTrigger.gameObject.AddComponent<CollisionNotifier>().Initialize(this.OnTableTennisTrigerredHandler, true);
        }

        private void OnTableTennisTrigerredHandler() {
            if(this._animator == null) {
                return;
            }
            this.ShowTTRackets(true);
            this._animator.Play("IndoorIntro");
        }

        private void ShowTTRackets(bool show) {
            foreach(GameObject ttRacket in _tableTennisRackets) {
                ttRacket.SetActive(show);
            }
        }


    }


}
