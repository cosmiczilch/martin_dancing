using Common;
using UnityEngine;

namespace Scenes {

    public class UniversitySceneManager : MonoBehaviour {

        [SerializeField] private Transform _computerInactiveHarness;

        [SerializeField] private Transform _computerOnTriggerHarness;
        [SerializeField] private Transform _computerOffTriggerHarness;

        private void Start() {
            if (this._computerInactiveHarness != null) {
                this._computerInactiveHarness.gameObject.SetActive(true);
            }

            if (this._computerOnTriggerHarness != null) {
                this._computerOnTriggerHarness.gameObject.AddComponent<CollisionNotifier>().Initialize(this.OnComputerOn, true);
            }

            if (this._computerOffTriggerHarness != null) {
                this._computerOffTriggerHarness.gameObject.AddComponent<CollisionNotifier>().Initialize(this.OnComputerOff, true);
            }
        }

        private void OnComputerOn() {
            if (this._computerInactiveHarness != null) {
                this._computerInactiveHarness.gameObject.SetActive(false);
            }
        }

        private void OnComputerOff() {
            if (this._computerInactiveHarness != null) {
                this._computerInactiveHarness.gameObject.SetActive(true);
            }
            GameObject[] gos = GameObject.FindGameObjectsWithTag("ComputerActive");
            foreach (GameObject go in gos) {
                GameObject.Destroy(go);
            }
        }
    }

}
