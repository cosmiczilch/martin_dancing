using Common;
using UnityEngine;

namespace Scenes {

    public class BeerHotDogComboPopup : MonoBehaviour {

        [SerializeField] private Transform _beerHotdogCombo = null;

        private void Start() {
            if (this._beerHotdogCombo != null) {
                this._beerHotdogCombo.gameObject.SetActive(false);
            }
            this.gameObject.AddComponent<CollisionNotifier>().Initialize(this.OnCollisionCallback, true);
        }

        private void OnCollisionCallback() {
            if (this._beerHotdogCombo != null) {
                this._beerHotdogCombo.gameObject.SetActive(true);
            }
        }
    }
}