using Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scenes {

    public class ArcadeMachine : MonoBehaviour {

        [SerializeField] private Image _machineOffImage;
        [SerializeField] private Image _machineOnImage;

        [SerializeField] private TextMeshProUGUI _nameText;

        [SerializeField] private Transform _proximityTriggerHarness;

        [SerializeField] private GameObject _coinPrefab;

        private void Start() {
            this._machineOffImage.gameObject.SetActive(true);
            this._nameText.gameObject.SetActive(false);
            this._machineOnImage.gameObject.SetActive(false);

            this._proximityTriggerHarness.gameObject.AddComponent<CollisionNotifier>().Initialize(this.OnProximityTrigerredHandler, true);
        }

        private void OnProximityTrigerredHandler() {
            this._machineOffImage.gameObject.SetActive(false);
            this._nameText.gameObject.SetActive(true);
            this._machineOnImage.gameObject.SetActive(true);

            if (this._coinPrefab != null) {
                GameObject.Instantiate(this._coinPrefab, this.gameObject.transform);
            }
        }
    }
}