﻿using TimiShared.Debug;
using TimiShared.Loading;
using TimiShared.Utils;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class Shortcuts : ScriptableObject {

    [MenuItem ("Martin Dancing/Shortcuts/Switch to Login scene #&1")]
    static void SwitchToLoginScene() {
        SwitchScene("Assets/Scenes/LoginScene.unity");
    }

    [MenuItem ("Martin Dancing/Shortcuts/Switch to MainMenu scene #&2")]
    static void SwitchToMainScene() {
        SwitchScene("Assets/Scenes/MainMenuScene.unity");
    }

    [MenuItem ("Martin Dancing/Shortcuts/Switch to TestUI scene #&9")]
    static void SwitchToTestUIScene() {
        SwitchScene("Assets/Scenes/TestUIScene.unity");
    }

    private static void SwitchScene(string sceneName) {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(sceneName);
    }

    [MenuItem ("Martin Dancing/Shortcuts/Copy Edited Books To First Launch #&p")]
    static void CopyEditedBooksToFirstLaunch() {

        TimiURI source = new TimiURI(FileBasePathType.LocalPersistentDataPath, "Books/Book of Martin.json");
        if (!FileUtils.DoesFileExist(source)) {
            TimiDebug.LogWarningColor("Book doesn't exist in workspace", LogColor.orange);
            return;
        }
        TimiURI destination = new TimiURI(FileBasePathType.LocalStreamingAssetsPath, "FirstLaunchData/LaunchInstance_0/Books/Book of Martin.json");

        if (FileUtils.DoesFileExist(destination)) {
            FileUtil.DeleteFileOrDirectory(destination.GetFullPath());
        }

        FileUtil.CopyFileOrDirectory(source.GetFullPath(), destination.GetFullPath());
        TimiDebug.LogColor("Copied!", LogColor.green);
    }

    [MenuItem ("Martin Dancing/Shortcuts/Copy First Launch Books To Workspace #&f")]
    static void CopyBookFromFirstLaunchToWorkspace() {
        TimiURI source = new TimiURI(FileBasePathType.LocalStreamingAssetsPath, "FirstLaunchData/LaunchInstance_0/Books/Book of Martin.json");
        if (!FileUtils.DoesFileExist(source)) {
            TimiDebug.LogWarningColor("Book doesn't exist in first launch data", LogColor.orange);
            return;
        }

        TimiURI destinationFolderUri = new TimiURI(FileBasePathType.LocalPersistentDataPath, "Books");
        if (!FileUtils.DoesDirectoryExist(destinationFolderUri)) {
            FileUtils.CreateDirectory(destinationFolderUri);
        }
        TimiURI destination = new TimiURI(FileBasePathType.LocalPersistentDataPath, "Books/Book of Martin.json");


        if (FileUtils.DoesFileExist(destination)) {
            FileUtil.DeleteFileOrDirectory(destination.GetFullPath());
        }

        FileUtil.CopyFileOrDirectory(source.GetFullPath(), destination.GetFullPath());
        TimiDebug.LogColor("Copied!", LogColor.green);
    }

}
