using System.Collections.Generic;
using TimiNarrative.DataModel;
using TimiShared.Instance;
using UnityEngine;

public class AppDataModels : DownloadableDataModelsContainer, IInstance {

    #region Data Models
    [SerializeField] private AppConstantsDataModel _appConstants = new AppConstantsDataModel();
    [SerializeField] private BookDetailsListDataModel _bookDetailList = new BookDetailsListDataModel();
    #endregion

    public override List<DownloadableDataModelBase> GetDataModels() {
        return new List<DownloadableDataModelBase>{
            this._appConstants,
            this._bookDetailList
            // Add new Data Models here
        };
    }

    public static AppDataModels Instance {
        get {
            return InstanceLocator.Instance<AppDataModels>();
        }
    }

    #region Public API
    public AppConstantsWrapper AppConstants {
        get; private set;
    }

    public BookDetailsWrapper BookDetailsList {
        get; private set;
    }
    #endregion

    private void Awake() {
        InstanceLocator.RegisterInstance<AppDataModels>(this);

        this._appConstants.OnDataModelUpdated += this.HandleAppConstantsDataModelUpdated;
        this._bookDetailList.OnDataModelUpdated += this.HandleBookDetailsListDataModelUpdated;
    }

    private void HandleAppConstantsDataModelUpdated() {
        this.AppConstants = new AppConstantsWrapper(this._appConstants);
    }

    private void HandleBookDetailsListDataModelUpdated() {
        this.BookDetailsList = new BookDetailsWrapper(this._bookDetailList);
    }
}