
public class AppConstantsWrapper {

    private readonly AppConstantsDataModel _data;

    public AppConstantsWrapper(AppConstantsDataModel data) {
        this._data = data;
    }

    #region Public API
    public float GetTest() {
        return this._data.test;
    }
    #endregion

}
